# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

file = File.read('jsondata.json')
profile_xm = JSON.parse(file)  
 
file = File.read('jsondata_t.json')
profile_tim = JSON.parse(file)

# file = File.read('1.json')
pub_01 = JSON.parse(File.read('1.json'))
Publication.create!(publication_hash:pub_01['data'],
                    published_date:pub_01['data']['attributes']['published_date'][0..3])

pub_02 = JSON.parse(File.read('2.json'))
Publication.create!(publication_hash:pub_02['data'],
                    published_date:pub_02['data']['attributes']['published_date'][0..3])

pub_07 = JSON.parse(File.read('7.json'))
Publication.create!(publication_hash:pub_07['data'],
                    published_date:pub_07['data']['attributes']['published_date'][0..3])
pub_08 = JSON.parse(File.read('8.json'))
Publication.create!(publication_hash:pub_08['data'],
                    published_date:pub_08['data']['attributes']['published_date'][0..3])
pub_09 = JSON.parse(File.read('9.json'))
Publication.create!(publication_hash:pub_09['data'],
                    published_date:pub_09['data']['attributes']['published_date'][0..3])
pub_10 = JSON.parse(File.read('10.json'))
Publication.create!(publication_hash:pub_10['data'],
                    published_date:pub_10['data']['attributes']['published_date'][0..3])
pub_11 = JSON.parse(File.read('11.json'))
Publication.create!(publication_hash:pub_11['data'],
                    published_date:pub_11['data']['attributes']['published_date'][0..3])
pub_12 = JSON.parse(File.read('12.json'))
Publication.create!(publication_hash:pub_12['data'],
                    published_date:pub_12['data']['attributes']['published_date'][0..3])
pub_15 = JSON.parse(File.read('15.json'))
Publication.create!(publication_hash:pub_15['data'],
                    published_date:pub_15['data']['attributes']['published_date'][0..3])
pub_16 = JSON.parse(File.read('16.json'))
Publication.create!(publication_hash:pub_16['data'],
                    published_date:pub_16['data']['attributes']['published_date'][0..3])
pub_17 = JSON.parse(File.read('17.json'))
Publication.create!(publication_hash:pub_17['data'],
                    published_date:pub_17['data']['attributes']['published_date'][0..3])
pub_18 = JSON.parse(File.read('18.json'))
Publication.create!(publication_hash:pub_18['data'],
                    published_date:pub_18['data']['attributes']['published_date'][0..3])
pub_19 = JSON.parse(File.read('19.json'))
Publication.create!(publication_hash:pub_19['data'],
                    published_date:pub_19['data']['attributes']['published_date'][0..3])
pub_20 = JSON.parse(File.read('20.json'))
Publication.create!(publication_hash:pub_20['data'],
                    published_date:pub_20['data']['attributes']['published_date'][0..3])
pub_21 = JSON.parse(File.read('21.json'))
Publication.create!(publication_hash:pub_21['data'],
                    published_date:pub_21['data']['attributes']['published_date'][0..3])
pub_22 = JSON.parse(File.read('22.json'))
Publication.create!(publication_hash:pub_22['data'],
                    published_date:pub_22['data']['attributes']['published_date'][0..3])
pub_23 = JSON.parse(File.read('23.json'))
Publication.create!(publication_hash:pub_23['data'],
                    published_date:pub_23['data']['attributes']['published_date'][0..3])
pub_24 = JSON.parse(File.read('24.json'))
Publication.create!(publication_hash:pub_24['data'],
                    published_date:pub_24['data']['attributes']['published_date'][0..3])
pub_25 = JSON.parse(File.read('25.json'))
Publication.create!(publication_hash:pub_25['data'],
                    published_date:pub_25['data']['attributes']['published_date'][0..3])
pub_26 = JSON.parse(File.read('26.json'))
Publication.create!(publication_hash:pub_26['data'],
                    published_date:pub_26['data']['attributes']['published_date'][0..3])
pub_27 = JSON.parse(File.read('27.json'))
Publication.create!(publication_hash:pub_27['data'],
                    published_date:pub_27['data']['attributes']['published_date'][0..3])
pub_28 = JSON.parse(File.read('28.json'))
Publication.create!(publication_hash:pub_28['data'],
                    published_date:pub_28['data']['attributes']['published_date'][0..3])
pub_29 = JSON.parse(File.read('29.json'))
Publication.create!(publication_hash:pub_29['data'],
                    published_date:pub_29['data']['attributes']['published_date'][0..3])


User.create!(name:  "Xiaoming Hu",
             email: "nande_liu@163.com",
             password:              "111111",
             password_confirmation: "111111",
             admin:true,
             profile: profile_xm 
             )
             
User.create!(name:  "Tim Johann",
             email: "hu.struppi@gmail.com",
             password:              "111111",
             password_confirmation: "111111", 
             profile: profile_tim )




50.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@hits.org"
  password = "password"
  User.create!(name:  name, 
               email: email,
               password:              password,
               password_confirmation: password)
end

# users = User.order(:created_at).take(6)
# 20.times do
  # content = Faker::Lorem.sentence(5)
  # users.each { |user| user.microposts.create!(content: content) }
# end


users = User.order(:created_at).take(6)
20.times do
  title = Faker::Lorem.sentence(5)
  content = Faker::Lorem.paragraphs
  users.each { |user| user.posts.create!(title: title, content: content) }
end

users = User.all

posts = Post.order(:created_at).take(5)
content = Faker::Lorem.sentence(5)

posts.each do |post|
post.comments.create!(user_id: '1', content: content) 
end

3.times do
    posts.each do |post|
     user_id = users.to_a.sample.id
     content = Faker::Lorem.sentence(8)
     post.comments.create!(user_id: user_id, content: content) end
 end


# Following relationships
users = User.all
user  = users.first
following = users[2..10]
followers = users[5..15]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }