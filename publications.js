var rows = [
    {
        "data":{
            "id":"1",
            "type":"publications",
            "attributes":{
                "title":"How do aminoadamantanes block the influenza M2 channel, and how does resistance develop?",
                "journal":"J Am Chem Soc",
                "published_date":"2011-05-04",
                "doi":null,
                "pubmed_id":21534619,
                "abstract":"The interactions between channels and their cognate blockers are at the heart of numerous biomedical phenomena. Herein, we unravel one particularly important example bearing direct pharmaceutical relevance: the blockage mechanism of the influenza M2 channel by the anti-flu amino-adamantyls (amantadine and rimantadine) and how the channel and, consequently, the virus develop resistance against them. Using both computational analyses and experimental verification, we find that amino-adamantyls inhibit M2's H(+) channel activity by electrostatic hindrance due to their positively charged amino group. In contrast, the hydrophobic adamantyl moiety on its own does not impact conductivity. Additionally, we were able to uncover how mutations in M2 are capable of retaining drug binding on the one hand yet rendering the protein and the mutated virus resistant to amino-adamantyls on the other hand. We show that the mutated, drug-resistant protein has a larger binding pocket for the drug. Hence, despite binding the channel, the drug remains sufficiently mobile so as not to exert a H(+)-blocking positive electrostatic hindrance. Such insight into the blocking mechanism of amino-adamantyls, and resistance thereof, may aid in the design of next-generation anti-flu agents.",
                "citation":"J Am Chem Soc. 2011 Jun 29;133(25):9903-11. doi: 10.1021/ja202288m. Epub 2011 May 16.",
                "link_to_pub":"https://www.ncbi.nlm.nih.gov/pubmed/21534619",
                "authors":[
                    "Hadas Leonov",
                    "P. Astrahan",
                    "M. Krugliak",
                    "I. T. Arkin"
                ]
            },
            "relationships":{
                "people":{
                    "data":[
                        {
                            "id":"1",
                            "type":"people"
                        }
                    ]
                },
                "projects":{
                    "data":[
                        {
                            "id":"1",
                            "type":"projects"
                        }
                    ]
                },
                "investigations":{
                    "data":[

                    ]
                },
                "studies":{
                    "data":[

                    ]
                },
                "assays":{
                    "data":[

                    ]
                },
                "data_files":{
                    "data":[

                    ]
                },
                "models":{
                    "data":[

                    ]
                },
                "publications":{
                    "data":[

                    ]
                },
                "presentations":{
                    "data":[

                    ]
                },
                "events":{
                    "data":[

                    ]
                }
            },
            "links":{
                "self":"/publications/1"
            },
            "meta":{
                "created":"2017-07-06T08:47:07.000Z",
                "modified":"2017-07-06T08:47:07.000Z",
                "api_version":"0.1",
                "uuid":"9df97940-4455-0135-b02b-0242ac120005",
                "base_url":"https://sandbox7.fairdomhub.org"
            }
        },
        "jsonapi":{
            "version":"1.0"
        }
    },

    {
        "data":{
            "id":"2",
            "type":"publications",
            "attributes":{
                "title":"Calculation of binding free energies.",
                "journal":"Methods Mol Biol",
                "published_date":"2014-10-22",
                "doi":null,
                "pubmed_id":25330964,
                "abstract":"Molecular dynamics simulations enable access to free energy differences governing the driving force underlying all biological processes. In the current chapter we describe alchemical methods allowing the calculation of relative free energy differences. We concentrate on the binding free energies that can be obtained using non-equilibrium approaches based on the Crooks Fluctuation Theorem. Together with the theoretical background, the chapter covers practical aspects of hybrid topology generation, simulation setup, and free energy estimation. An important aspect of the validation of a simulation setup is illustrated by means of calculating free energy differences along a full thermodynamic cycle. We provide a number of examples, including protein-ligand and protein-protein binding as well as ligand solvation free energy calculations.",
                "citation":"Methods Mol Biol. 2015;1215:173-209. doi: 10.1007/978-1-4939-1465-4_9.",
                "link_to_pub":"https://www.ncbi.nlm.nih.gov/pubmed/25330964",
                "authors":[
                    "V. Gapsys",
                    "S. Michielssens",
                    "J. H. Peters",
                    "B. L. de Groot",
                    "Hadas Leonov"
                ]
            },
            "relationships":{
                "people":{
                    "data":[
                        {
                            "id":"1",
                            "type":"people"
                        }
                    ]
                },
                "projects":{
                    "data":[
                        {
                            "id":"1",
                            "type":"projects"
                        },
                        {
                            "id":"3",
                            "type":"projects"
                        }
                    ]
                },
                "investigations":{
                    "data":[
                        {
                            "id":"4",
                            "type":"investigations"
                        }
                    ]
                },
                "studies":{
                    "data":[

                    ]
                },
                "assays":{
                    "data":[

                    ]
                },
                "data_files":{
                    "data":[

                    ]
                },
                "models":{
                    "data":[

                    ]
                },
                "publications":{
                    "data":[

                    ]
                },
                "presentations":{
                    "data":[

                    ]
                },
                "events":{
                    "data":[

                    ]
                }
            },
            "links":{
                "self":"/publications/2"
            },
            "meta":{
                "created":"2017-07-06T08:47:51.000Z",
                "modified":"2017-07-06T08:47:51.000Z",
                "api_version":"0.1",
                "uuid":"b80c7a00-4455-0135-b02c-0242ac120005",
                "base_url":"https://sandbox7.fairdomhub.org"
            }
        },
        "jsonapi":{
            "version":"1.0"
        }
    },

    {
        "data":{
            "id":"7",
            "type":"publications",
            "attributes":{
                "title":"The evolution of standards and data management practices in systems biology",
                "journal":"Molecular Systems Biology",
                "published_date":"2015-12-08",
                "doi":"10.15252/msb.20156053",
                "pubmed_id":null,
                "abstract":null,
                "citation":"Molecular Systems Biology 11(12) : 851",
                "link_to_pub":"https://www.ncbi.nlm.nih.gov/pubmed/",
                "authors":[
                    "N. J. Stanford",
                    "K. Wolstencroft",
                    "M. Golebiewski",
                    "R. Kania",
                    "N. Juty",
                    "C. Tomlinson",
                    "S. Owen",
                    "S. Butcher",
                    "H. Hermjakob",
                    "N. Le Novere",
                    "Wolfgang Müller",
                    "J. Snoep",
                    "C. Goble"
                ]
            },
            "relationships":{
                "people":{
                    "data":[
                        {
                            "id":"33",
                            "type":"people"
                        }
                    ]
                },
                "projects":{
                    "data":[
                        {
                            "id":"3",
                            "type":"projects"
                        }
                    ]
                },
                "investigations":{
                    "data":[
                        {
                            "id":"3",
                            "type":"investigations"
                        }
                    ]
                },
                "studies":{
                    "data":[
                        {
                            "id":"1",
                            "type":"studies"
                        }
                    ]
                },
                "assays":{
                    "data":[

                    ]
                },
                "data_files":{
                    "data":[

                    ]
                },
                "models":{
                    "data":[

                    ]
                },
                "publications":{
                    "data":[

                    ]
                },
                "presentations":{
                    "data":[

                    ]
                },
                "events":{
                    "data":[

                    ]
                }
            },
            "links":{
                "self":"/publications/7"
            },
            "meta":{
                "created":"2017-10-06T12:55:10.000Z",
                "modified":"2017-10-06T12:55:24.000Z",
                "api_version":"0.1",
                "uuid":"82c5acf0-8cc3-0135-b033-0242ac120005",
                "base_url":"https://sandbox7.fairdomhub.org"
            }
        },
        "jsonapi":{
            "version":"1.0"
        }
    },

    {
        "data":{
            "id":"8",
            "type":"publications",
            "attributes":{
                "title":"SABIO-RK--database for biochemical reaction kinetics.",
                "journal":"Nucleic Acids Res",
                "published_date":"2011-11-22",
                "doi":null,
                "pubmed_id":22102587,
                "abstract":"SABIO-RK (http://sabio.h-its.org/) is a web-accessible database storing comprehensive information about biochemical reactions and their kinetic properties. SABIO-RK offers standardized data manually extracted from the literature and data directly submitted from lab experiments. The database content includes kinetic parameters in relation to biochemical reactions and their biological sources with no restriction on any particular set of organisms. Additionally, kinetic rate laws and corresponding equations as well as experimental conditions are represented. All the data are manually curated and annotated by biological experts, supported by automated consistency checks. SABIO-RK can be accessed via web-based user interfaces or automatically via web services that allow direct data access by other tools. Both interfaces support the export of the data together with its annotations in SBML (Systems Biology Markup Language), e.g. for import in modelling tools.",
                "citation":"Nucleic Acids Res. 2012 Jan;40(Database issue):D790-6. doi: 10.1093/nar/gkr1046.  Epub 2011 Nov 18.",
                "link_to_pub":"https://www.ncbi.nlm.nih.gov/pubmed/22102587",
                "authors":[
                    "Ulrike Wittig",
                    "R. Kania",
                    "Martin Golebiewski",
                    "Maja Rey",
                    "L. Shi",
                    "L. Jong",
                    "E. Algaa",
                    "A. Weidemann",
                    "H. Sauer-Danzwith",
                    "S. Mir",
                    "Olga Krebs",
                    "M. Bittkowski",
                    "E. Wetsch",
                    "I. Rojas",
                    "Wolfgang Müller"
                ]
            },
            "relationships":{
                "people":{
                    "data":[
                        {
                            "id":"33",
                            "type":"people"
                        },
                        {
                            "id":"35",
                            "type":"people"
                        },
                        {
                            "id":"37",
                            "type":"people"
                        },
                        {
                            "id":"38",
                            "type":"people"
                        },
                        {
                            "id":"39",
                            "type":"people"
                        }
                    ]
                },
                "projects":{
                    "data":[
                        {
                            "id":"3",
                            "type":"projects"
                        }
                    ]
                },
                "investigations":{
                    "data":[

                    ]
                },
                "studies":{
                    "data":[
                        {
                            "id":"2",
                            "type":"studies"
                        }
                    ]
                },
                "assays":{
                    "data":[
                        {
                            "id":"7",
                            "type":"assays"
                        },
                        {
                            "id":"8",
                            "type":"assays"
                        }
                    ]
                },
                "data_files":{
                    "data":[
                        {
                            "id":"10",
                            "type":"data_files"
                        },
                        {
                            "id":"11",
                            "type":"data_files"
                        },
                        {
                            "id":"26",
                            "type":"data_files"
                        },
                        {
                            "id":"27",
                            "type":"data_files"
                        },
                        {
                            "id":"28",
                            "type":"data_files"
                        },
                        {
                            "id":"29",
                            "type":"data_files"
                        },
                        {
                            "id":"30",
                            "type":"data_files"
                        },
                        {
                            "id":"31",
                            "type":"data_files"
                        },
                        {
                            "id":"32",
                            "type":"data_files"
                        }
                    ]
                },
                "models":{
                    "data":[

                    ]
                },
                "publications":{
                    "data":[

                    ]
                },
                "presentations":{
                    "data":[

                    ]
                },
                "events":{
                    "data":[

                    ]
                }
            },
            "links":{
                "self":"/publications/8"
            },
            "meta":{
                "created":"2017-10-06T13:45:26.000Z",
                "modified":"2017-10-06T13:45:26.000Z",
                "api_version":"0.1",
                "uuid":"8853af00-8cca-0135-3248-0242ac120005",
                "base_url":"https://sandbox7.fairdomhub.org"
            }
        },
        "jsonapi":{
            "version":"1.0"
        }
    },

    {
        "data":{
            "id":"9",
            "type":"publications",
            "attributes":{
                "title":"FAIRDOMHub: a repository and collaboration environment for sharing systems biology research",
                "journal":"Nucleic Acids Res",
                "published_date":"2017-01-03",
                "doi":"10.1093/nar/gkw1032",
                "pubmed_id":null,
                "abstract":null,
                "citation":"Nucleic Acids Res 45(D1) : D404",
                "link_to_pub":"https://www.ncbi.nlm.nih.gov/pubmed/",
                "authors":[
                    "Katherine Wolstencroft",
                    "Olga Krebs",
                    "Jacky L. Snoep",
                    "Natalie J. Stanford",
                    "Finn Bacall",
                    "Martin Golebiewski",
                    "Rostyk Kuzyakiv",
                    "Quyen Nguyen",
                    "Stuart Owen",
                    "Stian Soiland-Reyes",
                    "Jakub Straszewski",
                    "David D. van Niekerk",
                    "Alan R. Williams",
                    "Lars Malmström",
                    "Bernd Rinn",
                    "Wolfgang Müller",
                    "Carole Goble"
                ]
            },
            "relationships":{
                "people":{
                    "data":[
                        {
                            "id":"33",
                            "type":"people"
                        }
                    ]
                },
                "projects":{
                    "data":[
                        {
                            "id":"3",
                            "type":"projects"
                        }
                    ]
                },
                "investigations":{
                    "data":[

                    ]
                },
                "studies":{
                    "data":[

                    ]
                },
                "assays":{
                    "data":[

                    ]
                },
                "data_files":{
                    "data":[

                    ]
                },
                "models":{
                    "data":[

                    ]
                },
                "publications":{
                    "data":[

                    ]
                },
                "presentations":{
                    "data":[

                    ]
                },
                "events":{
                    "data":[

                    ]
                }
            },
            "links":{
                "self":"/publications/9"
            },
            "meta":{
                "created":"2017-10-06T13:57:37.000Z",
                "modified":"2017-10-06T13:57:37.000Z",
                "api_version":"0.1",
                "uuid":"3bfc3750-8ccc-0135-324a-0242ac120005",
                "base_url":"https://sandbox7.fairdomhub.org"
            }
        },
        "jsonapi":{
            "version":"1.0"
        }
    },

    {
        "data":{
            "id":"10",
            "type":"publications",
            "attributes":{
                "title":"Data extraction for the reaction kinetics database SABIO-RK",
                "journal":"Perspectives in Science",
                "published_date":"2014-05-01",
                "doi":"10.1016/j.pisc.2014.02.004",
                "pubmed_id":null,
                "abstract":"SABIO-RK (http://sabio.h-its.org/) is a web-accessible, manually curated database that has been established as a resource for biochemical reactions and their kinetic properties with a focus on supporting the computational modeling to create models of biochemical reaction networks. SABIO-RK data are mainly extracted from literature but also directly submitted from lab experiments. In most cases the information in the literature is distributed across the whole publication, insufficiently structured and often described without standard terminology. Therefore the manual extraction of knowledge from the literature requires biological experts to understand the paper and interpret the data. The database offers the literature data in a structured format including annotations to controlled vocabularies, ontologies and external databases which supports modellers, as well as experimentalists, in the very time consuming process of collecting information from different publications.\r\n\r\nHere we describe the data extraction and curation efforts needed for SABIO-RK and give recommendations for publishing kinetic data in a complete and structured manner.",
                "citation":"Perspectives in Science 1(1-6) : 33",
                "link_to_pub":"https://www.ncbi.nlm.nih.gov/pubmed/",
                "authors":[
                    "Ulrike Wittig",
                    "Renate Kania",
                    "Meik Bittkowski",
                    "Elina Wetsch",
                    "Lei Shi",
                    "Lenneke Jong",
                    "Martin Golebiewski",
                    "Maja Rey",
                    "Andreas Weidemann",
                    "Isabel Rojas",
                    "Wolfgang Müller"
                ]
            },
            "relationships":{
                "people":{
                    "data":[
                        {
                            "id":"33",
                            "type":"people"
                        },
                        {
                            "id":"35",
                            "type":"people"
                        },
                        {
                            "id":"37",
                            "type":"people"
                        }
                    ]
                },
                "projects":{
                    "data":[
                        {
                            "id":"3",
                            "type":"projects"
                        }
                    ]
                },
                "investigations":{
                    "data":[

                    ]
                },
                "studies":{
                    "data":[
                        {
                            "id":"2",
                            "type":"studies"
                        }
                    ]
                },
                "assays":{
                    "data":[
                        {
                            "id":"6",
                            "type":"assays"
                        }
                    ]
                },
                "data_files":{
                    "data":[
                        {
                            "id":"12",
                            "type":"data_files"
                        },
                        {
                            "id":"13",
                            "type":"data_files"
                        }
                    ]
                },
                "models":{
                    "data":[

                    ]
                },
                "publications":{
                    "data":[

                    ]
                },
                "presentations":{
                    "data":[

                    ]
                },
                "events":{
                    "data":[

                    ]
                }
            },
            "links":{
                "self":"/publications/10"
            },
            "meta":{
                "created":"2017-10-06T14:09:00.000Z",
                "modified":"2017-10-06T14:09:59.000Z",
                "api_version":"0.1",
                "uuid":"d3379c20-8ccd-0135-324b-0242ac120005",
                "base_url":"https://sandbox7.fairdomhub.org"
            }
        },
        "jsonapi":{
            "version":"1.0"
        }
    },

    {
        "data":{
            "id":"11",
            "type":"publications",
            "attributes":{
                "title":"Challenges for an enzymatic reaction kinetics database.",
                "journal":"FEBS J",
                "published_date":"2013-10-30",
                "doi":null,
                "pubmed_id":24165050,
                "abstract":"The scientific literature contains a tremendous amount of kinetic data describing the dynamic behaviour of biochemical reactions over time. These data are needed for computational modelling to create models of biochemical reaction networks and to obtain a better understanding of the processes in living cells. To extract the knowledge from the literature, biocurators are required to understand a paper and interpret the data. For modellers, as well as experimentalists, this process is very time consuming because the information is distributed across the publication and, in most cases, is insufficiently structured and often described without standard terminology. In recent years, biological databases for different data types have been developed. The advantages of these databases lie in their unified structure, searchability and the potential for augmented analysis by software, which supports the modelling process. We have developed the SABIO-RK database for biochemical reaction kinetics. In the present review, we describe the challenges for database developers and curators, beginning with an analysis of relevant publications up to the export of database information in a standardized format. The aim of the present review is to draw the experimentalist's attention to the problem (from a data integration point of view) of incompletely and imprecisely written publications. We describe how to lower the barrier to curators and improve this situation. At the same time, we are aware that curating experimental data takes time. There is a community concerned with making the task of publishing data with the proper structure and annotation to ontologies much easier. In this respect, we highlight some useful initiatives and tools.",
                "citation":"FEBS J. 2014 Jan;281(2):572-82. doi: 10.1111/febs.12562. Epub 2013 Oct 25.",
                "link_to_pub":"https://www.ncbi.nlm.nih.gov/pubmed/24165050",
                "authors":[
                    "Ulrike Wittig",
                    "Maja Rey",
                    "R. Kania",
                    "M. Bittkowski",
                    "L. Shi",
                    "M. Golebiewski",
                    "A. Weidemann",
                    "Wolfgang Müller",
                    "I. Rojas"
                ]
            },
            "relationships":{
                "people":{
                    "data":[
                        {
                            "id":"33",
                            "type":"people"
                        },
                        {
                            "id":"35",
                            "type":"people"
                        },
                        {
                            "id":"37",
                            "type":"people"
                        }
                    ]
                },
                "projects":{
                    "data":[
                        {
                            "id":"3",
                            "type":"projects"
                        }
                    ]
                },
                "investigations":{
                    "data":[

                    ]
                },
                "studies":{
                    "data":[
                        {
                            "id":"2",
                            "type":"studies"
                        }
                    ]
                },
                "assays":{
                    "data":[
                        {
                            "id":"6",
                            "type":"assays"
                        }
                    ]
                },
                "data_files":{
                    "data":[
                        {
                            "id":"12",
                            "type":"data_files"
                        },
                        {
                            "id":"13",
                            "type":"data_files"
                        },
                        {
                            "id":"15",
                            "type":"data_files"
                        },
                        {
                            "id":"16",
                            "type":"data_files"
                        }
                    ]
                },
                "models":{
                    "data":[

                    ]
                },
                "publications":{
                    "data":[

                    ]
                },
                "presentations":{
                    "data":[

                    ]
                },
                "events":{
                    "data":[

                    ]
                }
            },
            "links":{
                "self":"/publications/11"
            },
            "meta":{
                "created":"2017-10-06T14:14:35.000Z",
                "modified":"2017-10-06T14:14:35.000Z",
                "api_version":"0.1",
                "uuid":"9a9fe600-8cce-0135-324b-0242ac120005",
                "base_url":"https://sandbox7.fairdomhub.org"
            }
        },
        "jsonapi":{
            "version":"1.0"
        }
    },

    {
        "data":{
            "id":"12",
            "type":"publications",
            "attributes":{
                "title":"SEEK: a systems biology data and model management platform.",
                "journal":"BMC Syst Biol",
                "published_date":"2015-07-15",
                "doi":null,
                "pubmed_id":26160520,
                "abstract":"BACKGROUND: Systems biology research typically involves the integration and analysis of heterogeneous data types in order to model and predict biological processes. Researchers therefore require tools and resources to facilitate the sharing and integration of data, and for linking of data to systems biology models. There are a large number of public repositories for storing biological data of a particular type, for example transcriptomics or proteomics, and there are several model repositories. However, this silo-type storage of data and models is not conducive to systems biology investigations. Interdependencies between multiple omics datasets and between datasets and models are essential. Researchers require an environment that will allow the management and sharing of heterogeneous data and models in the context of the experiments which created them. RESULTS: The SEEK is a suite of tools to support the management, sharing and exploration of data and models in systems biology. The SEEK platform provides an access-controlled, web-based environment for scientists to share and exchange data and models for day-to-day collaboration and for public dissemination. A plug-in architecture allows the linking of experiments, their protocols, data, models and results in a configurable system that is available 'off the shelf'. Tools to run model simulations, plot experimental data and assist with data annotation and standardisation combine to produce a collection of resources that support analysis as well as sharing. Underlying semantic web resources additionally extract and serve SEEK metadata in RDF (Resource Description Format). SEEK RDF enables rich semantic queries, both within SEEK and between related resources in the web of Linked Open Data. CONCLUSION: The SEEK platform has been adopted by many systems biology consortia across Europe. It is a data management environment that has a low barrier of uptake and provides rich resources for collaboration. This paper provides an update on the functions and features of the SEEK software, and describes the use of the SEEK in the SysMO consortium (Systems biology for Micro-organisms), and the VLN (virtual Liver Network), two large systems biology initiatives with different research aims and different scientific communities.",
                "citation":"BMC Syst Biol. 2015 Jul 11;9:33. doi: 10.1186/s12918-015-0174-y.",
                "link_to_pub":"https://www.ncbi.nlm.nih.gov/pubmed/26160520",
                "authors":[
                    "K. Wolstencroft",
                    "S. Owen",
                    "O. Krebs",
                    "Q. Nguyen",
                    "N. J. Stanford",
                    "M. Golebiewski",
                    "A. Weidemann",
                    "M. Bittkowski",
                    "L. An",
                    "D. Shockley",
                    "J. L. Snoep",
                    "Wolfgang Müller",
                    "C. Goble"
                ]
            },
            "relationships":{
                "people":{
                    "data":[
                        {
                            "id":"33",
                            "type":"people"
                        }
                    ]
                },
                "projects":{
                    "data":[
                        {
                            "id":"3",
                            "type":"projects"
                        }
                    ]
                },
                "investigations":{
                    "data":[

                    ]
                },
                "studies":{
                    "data":[
                        {
                            "id":"1",
                            "type":"studies"
                        }
                    ]
                },
                "assays":{
                    "data":[

                    ]
                },
                "data_files":{
                    "data":[
                        {
                            "id":"1",
                            "type":"data_files"
                        }
                    ]
                },
                "models":{
                    "data":[

                    ]
                },
                "publications":{
                    "data":[

                    ]
                },
                "presentations":{
                    "data":[

                    ]
                },
                "events":{
                    "data":[

                    ]
                }
            },
            "links":{
                "self":"/publications/12"
            },
            "meta":{
                "created":"2017-10-06T14:24:13.000Z",
                "modified":"2017-10-06T14:24:13.000Z",
                "api_version":"0.1",
                "uuid":"f3138df0-8ccf-0135-324a-0242ac120005",
                "base_url":"https://sandbox7.fairdomhub.org"
            }
        },
        "jsonapi":{
            "version":"1.0"
        }
    },

    {
        "data":{
            "id":"15",
            "type":"publications",
            "attributes":{
                "title":"Web-based visualisation of the transcriptional control network of Escherichia coli.",
                "journal":"In Silico Biol",
                "published_date":"2004-10-28",
                "doi":null,
                "pubmed_id":15506999,
                "abstract":"Transcription is one of the basic processes of gene expression, controlled by a complex network of biochemical reactions. Despite its importance, most work on the visualisation of biochemical networks focuses on the representation of metabolic pathways. The visualisation of the complex networks controlling transcription requires the implementation of a hierarchical approach that allows the display of the structure of each regulatory region with its transcription factors and regulated operons. This paper presents a web-based application for the visualisation of transcriptional control networks. It takes as case study the organism Escherichia coli. The definition of the visual components implemented is mainly based on those proposed by Shen-Orr et al., 2002, slightly extended to visualise complex networks.",
                "citation":"In Silico Biol. 2004;4(4):507-15.",
                "link_to_pub":"https://www.ncbi.nlm.nih.gov/pubmed/15506999",
                "authors":[
                    "N. Sosa",
                    "A. Kremling",
                    "E. Ratsch",
                    "I. Rojas"
                ]
            },
            "relationships":{
                "people":{
                    "data":[
                        {
                            "id":"33",
                            "type":"people"
                        }
                    ]
                },
                "projects":{
                    "data":[
                        {
                            "id":"3",
                            "type":"projects"
                        }
                    ]
                },
                "investigations":{
                    "data":[

                    ]
                },
                "studies":{
                    "data":[

                    ]
                },
                "assays":{
                    "data":[

                    ]
                },
                "data_files":{
                    "data":[

                    ]
                },
                "models":{
                    "data":[

                    ]
                },
                "publications":{
                    "data":[

                    ]
                },
                "presentations":{
                    "data":[

                    ]
                },
                "events":{
                    "data":[

                    ]
                }
            },
            "links":{
                "self":"/publications/15"
            },
            "meta":{
                "created":"2017-10-12T15:32:56.000Z",
                "modified":"2017-10-12T15:32:56.000Z",
                "api_version":"0.1",
                "uuid":"8b90b0a0-9190-0135-324a-0242ac120005",
                "base_url":"https://sandbox7.fairdomhub.org"
            }
        },
        "jsonapi":{
            "version":"1.0"
        }
    },

    {
        "data":{
            "id":"15",
            "type":"publications",
            "attributes":{
                "title":"Web-based visualisation of the transcriptional control network of Escherichia coli.",
                "journal":"In Silico Biol",
                "published_date":"2004-10-28",
                "doi":null,
                "pubmed_id":15506999,
                "abstract":"Transcription is one of the basic processes of gene expression, controlled by a complex network of biochemical reactions. Despite its importance, most work on the visualisation of biochemical networks focuses on the representation of metabolic pathways. The visualisation of the complex networks controlling transcription requires the implementation of a hierarchical approach that allows the display of the structure of each regulatory region with its transcription factors and regulated operons. This paper presents a web-based application for the visualisation of transcriptional control networks. It takes as case study the organism Escherichia coli. The definition of the visual components implemented is mainly based on those proposed by Shen-Orr et al., 2002, slightly extended to visualise complex networks.",
                "citation":"In Silico Biol. 2004;4(4):507-15.",
                "link_to_pub":"https://www.ncbi.nlm.nih.gov/pubmed/15506999",
                "authors":[
                    "N. Sosa",
                    "A. Kremling",
                    "E. Ratsch",
                    "I. Rojas"
                ]
            },
            "relationships":{
                "people":{
                    "data":[
                        {
                            "id":"33",
                            "type":"people"
                        }
                    ]
                },
                "projects":{
                    "data":[
                        {
                            "id":"3",
                            "type":"projects"
                        }
                    ]
                },
                "investigations":{
                    "data":[

                    ]
                },
                "studies":{
                    "data":[

                    ]
                },
                "assays":{
                    "data":[

                    ]
                },
                "data_files":{
                    "data":[

                    ]
                },
                "models":{
                    "data":[

                    ]
                },
                "publications":{
                    "data":[

                    ]
                },
                "presentations":{
                    "data":[

                    ]
                },
                "events":{
                    "data":[

                    ]
                }
            },
            "links":{
                "self":"/publications/15"
            },
            "meta":{
                "created":"2017-10-12T15:32:56.000Z",
                "modified":"2017-10-12T15:32:56.000Z",
                "api_version":"0.1",
                "uuid":"8b90b0a0-9190-0135-324a-0242ac120005",
                "base_url":"https://sandbox7.fairdomhub.org"
            }
        },
        "jsonapi":{
            "version":"1.0"
        }
    },
    {
        "data":{
            "id":"16",
            "type":"publications",
            "attributes":{
                "title":"A-R-E: The Author-Review-Execute Environment",
                "journal":"Procedia Computer Science",
                "published_date":"2011-01-01",
                "doi":"10.1016/j.procs.2011.04.066",
                "pubmed_id":null,
                "abstract":"The Author-Review-Execute (A-R-E) is an innovative concept to offer under a single principle and platform an environment to support the life cycle of an (executable) paper; namely the authoring of the paper, its submission, the reviewing process, the author's revisions, its publication, and finally the study (reading/interaction) of the paper as well as extensions (follow ups) of the paper. It combines Semantic Wiki technology, a resolver that solves links both between parts of documents to executable code or to data, an anonymizing component to support the authoring and reviewing tasks, and web services providing link perennity.",
                "citation":"Procedia Computer Science 4 : 627",
                "link_to_pub":"https://www.ncbi.nlm.nih.gov/pubmed/",
                "authors":[
                    "Wolfgang Müller",
                    "Isabel Rojas",
                    "Andreas Eberhart",
                    "Peter Haase",
                    "Michael Schmidt"
                ]
            },
            "relationships":{
                "people":{
                    "data":[
                        {
                            "id":"33",
                            "type":"people"
                        }
                    ]
                },
                "projects":{
                    "data":[
                        {
                            "id":"3",
                            "type":"projects"
                        }
                    ]
                },
                "investigations":{
                    "data":[

                    ]
                },
                "studies":{
                    "data":[

                    ]
                },
                "assays":{
                    "data":[

                    ]
                },
                "data_files":{
                    "data":[

                    ]
                },
                "models":{
                    "data":[

                    ]
                },
                "publications":{
                    "data":[

                    ]
                },
                "presentations":{
                    "data":[

                    ]
                },
                "events":{
                    "data":[

                    ]
                }
            },
            "links":{
                "self":"/publications/16"
            },
            "meta":{
                "created":"2017-10-12T15:36:11.000Z",
                "modified":"2017-10-12T15:36:32.000Z",
                "api_version":"0.1",
                "uuid":"ffc95a60-9190-0135-324b-0242ac120005",
                "base_url":"https://sandbox7.fairdomhub.org"
            }
        },
        "jsonapi":{
            "version":"1.0"
        }
    },

    {
        "data":{
            "id":"17",
            "type":"publications",
            "attributes":{
                "title":"Data management and data enrichment for systems biology projects.",
                "journal":"J Biotechnol",
                "published_date":"2017-06-14",
                "doi":null,
                "pubmed_id":28606610,
                "abstract":"Collecting, curating, interlinking, and sharing high quality data are central to de.NBI-SysBio, the systems biology data management service center within the de.NBI network (German Network for Bioinformatics Infrastructure). The work of the center is guided by the FAIR principles for scientific data management and stewardship. FAIR stands for the four foundational principles Findability, Accessibility, Interoperability, and Reusability which were established to enhance the ability of machines to automatically find, access, exchange and use data. Within this overview paper we describe three tools (SABIO-RK, Excemplify, SEEK) that exemplify the contribution of de.NBI-SysBio services to FAIR data, models, and experimental methods storage and exchange. The interconnectivity of the tools and the data workflow within systems biology projects will be explained. For many years we are the German partner in the FAIRDOM initiative (http://fair-dom.org) to establish a European data and model management service facility for systems biology.",
                "citation":"J Biotechnol. 2017 Nov 10;261:229-237. doi: 10.1016/j.jbiotec.2017.06.007. Epub 2017 Jun 10.",
                "link_to_pub":"https://www.ncbi.nlm.nih.gov/pubmed/28606610",
                "authors":[
                    "Ulrike Wittig",
                    "Maja Rey",
                    "A. Weidemann",
                    "Wolfgang Müller"
                ]
            },
            "relationships":{
                "people":{
                    "data":[
                        {
                            "id":"33",
                            "type":"people"
                        },
                        {
                            "id":"35",
                            "type":"people"
                        },
                        {
                            "id":"37",
                            "type":"people"
                        }
                    ]
                },
                "projects":{
                    "data":[
                        {
                            "id":"3",
                            "type":"projects"
                        }
                    ]
                },
                "investigations":{
                    "data":[

                    ]
                },
                "studies":{
                    "data":[
                        {
                            "id":"6",
                            "type":"studies"
                        }
                    ]
                },
                "assays":{
                    "data":[

                    ]
                },
                "data_files":{
                    "data":[
                        {
                            "id":"17",
                            "type":"data_files"
                        },
                        {
                            "id":"18",
                            "type":"data_files"
                        },
                        {
                            "id":"19",
                            "type":"data_files"
                        },
                        {
                            "id":"20",
                            "type":"data_files"
                        },
                        {
                            "id":"21",
                            "type":"data_files"
                        },
                        {
                            "id":"22",
                            "type":"data_files"
                        },
                        {
                            "id":"23",
                            "type":"data_files"
                        },
                        {
                            "id":"24",
                            "type":"data_files"
                        },
                        {
                            "id":"25",
                            "type":"data_files"
                        }
                    ]
                },
                "models":{
                    "data":[

                    ]
                },
                "publications":{
                    "data":[

                    ]
                },
                "presentations":{
                    "data":[

                    ]
                },
                "events":{
                    "data":[

                    ]
                }
            },
            "links":{
                "self":"/publications/17"
            },
            "meta":{
                "created":"2017-11-08T10:41:19.000Z",
                "modified":"2017-11-08T10:41:19.000Z",
                "api_version":"0.1",
                "uuid":"47719480-a69f-0135-324b-0242ac120005",
                "base_url":"https://sandbox7.fairdomhub.org"
            }
        },
        "jsonapi":{
            "version":"1.0"
        }
    },

    {
        "data":{
            "id":"18",
            "type":"publications",
            "attributes":{
                "title":"Enzyme kinetics informatics: from instrument to browser.",
                "journal":"FEBS J",
                "published_date":"2010-08-27",
                "doi":null,
                "pubmed_id":20738395,
                "abstract":"A limited number of publicly available resources provide access to enzyme kinetic parameters. These have been compiled through manual data mining of published papers, not from the original, raw experimental data from which the parameters were calculated. This is largely due to the lack of software or standards to support the capture, analysis, storage and dissemination of such experimental data. Introduced here is an integrative system to manage experimental enzyme kinetics data from instrument to browser. The approach is based on two interrelated databases: the existing SABIO-RK database, containing kinetic data and corresponding metadata, and the newly introduced experimental raw data repository, MeMo-RK. Both systems are publicly available by web browser and web service interfaces and are configurable to ensure privacy of unpublished data. Users of this system are provided with the ability to view both kinetic parameters and the experimental raw data from which they are calculated, providing increased confidence in the data. A data analysis and submission tool, the kineticswizard, has been developed to allow the experimentalist to perform data collection, analysis and submission to both data resources. The system is designed to be extensible, allowing integration with other manufacturer instruments covering a range of analytical techniques.",
                "citation":"FEBS J. 2010 Sep;277(18):3769-79. doi: 10.1111/j.1742-4658.2010.07778.x. Epub 2010 Aug 3.",
                "link_to_pub":"https://www.ncbi.nlm.nih.gov/pubmed/20738395",
                "authors":[
                    "N. Swainston",
                    "M. Golebiewski",
                    "H. L. Messiha",
                    "N. Malys",
                    "R. Kania",
                    "S. Kengne",
                    "O. Krebs",
                    "S. Mir",
                    "H. Sauer-Danzwith",
                    "K. Smallbone",
                    "A. Weidemann",
                    "Ulrike Wittig",
                    "D. B. Kell",
                    "P. Mendes",
                    "Wolfgang Müller",
                    "N. W. Paton",
                    "I. Rojas"
                ]
            },
            "relationships":{
                "people":{
                    "data":[
                        {
                            "id":"33",
                            "type":"people"
                        },
                        {
                            "id":"35",
                            "type":"people"
                        }
                    ]
                },
                "projects":{
                    "data":[
                        {
                            "id":"3",
                            "type":"projects"
                        }
                    ]
                },
                "investigations":{
                    "data":[

                    ]
                },
                "studies":{
                    "data":[

                    ]
                },
                "assays":{
                    "data":[

                    ]
                },
                "data_files":{
                    "data":[

                    ]
                },
                "models":{
                    "data":[

                    ]
                },
                "publications":{
                    "data":[

                    ]
                },
                "presentations":{
                    "data":[

                    ]
                },
                "events":{
                    "data":[

                    ]
                }
            },
            "links":{
                "self":"/publications/18"
            },
            "meta":{
                "created":"2017-11-08T10:42:16.000Z",
                "modified":"2017-11-08T10:42:16.000Z",
                "api_version":"0.1",
                "uuid":"696d3570-a69f-0135-324b-0242ac120005",
                "base_url":"https://sandbox7.fairdomhub.org"
            }
        },
        "jsonapi":{
            "version":"1.0"
        }
    },

    {
        "data":{
            "id":"19",
            "type":"publications",
            "attributes":{
                "title":"SABIO-RK: an updated resource for manually curated biochemical reaction kinetics.",
                "journal":"Nucleic Acids Res",
                "published_date":"2017-11-02",
                "doi":null,
                "pubmed_id":29092055,
                "abstract":"SABIO-RK (http://sabiork.h-its.org/) is a manually curated database containing data about biochemical reactions and their reaction kinetics. The data are primarily extracted from scientific literature and stored in a relational database. The content comprises both naturally occurring and alternatively measured biochemical reactions and is not restricted to any organism class. The data are made available to the public by a web-based search interface and by web services for programmatic access. In this update we describe major improvements and extensions of SABIO-RK since our last publication in the database issue of Nucleic Acid Research (2012). (i) The website has been completely revised and (ii) allows now also free text search for kinetics data. (iii) Additional interlinkages with other databases in our field have been established; this enables users to gain directly comprehensive knowledge about the properties of enzymes and kinetics beyond SABIO-RK. (iv) Vice versa, direct access to SABIO-RK data has been implemented in several systems biology tools and workflows. (v) On request of our experimental users, the data can be exported now additionally in spreadsheet formats. (vi) The newly established SABIO-RK Curation Service allows to respond to specific data requirements.",
                "citation":"Nucleic Acids Res. 2017 Oct 30. doi: 10.1093/nar/gkx1065.",
                "link_to_pub":"https://www.ncbi.nlm.nih.gov/pubmed/29092055",
                "authors":[
                    "Ulrike Wittig",
                    "Maja Rey",
                    "A. Weidemann",
                    "R. Kania",
                    "Wolfgang Müller"
                ]
            },
            "relationships":{
                "people":{
                    "data":[
                        {
                            "id":"33",
                            "type":"people"
                        },
                        {
                            "id":"35",
                            "type":"people"
                        },
                        {
                            "id":"37",
                            "type":"people"
                        }
                    ]
                },
                "projects":{
                    "data":[
                        {
                            "id":"3",
                            "type":"projects"
                        }
                    ]
                },
                "investigations":{
                    "data":[

                    ]
                },
                "studies":{
                    "data":[
                        {
                            "id":"2",
                            "type":"studies"
                        }
                    ]
                },
                "assays":{
                    "data":[
                        {
                            "id":"7",
                            "type":"assays"
                        },
                        {
                            "id":"8",
                            "type":"assays"
                        }
                    ]
                },
                "data_files":{
                    "data":[
                        {
                            "id":"10",
                            "type":"data_files"
                        },
                        {
                            "id":"11",
                            "type":"data_files"
                        },
                        {
                            "id":"33",
                            "type":"data_files"
                        }
                    ]
                },
                "models":{
                    "data":[

                    ]
                },
                "publications":{
                    "data":[

                    ]
                },
                "presentations":{
                    "data":[

                    ]
                },
                "events":{
                    "data":[

                    ]
                }
            },
            "links":{
                "self":"/publications/19"
            },
            "meta":{
                "created":"2017-11-08T10:43:00.000Z",
                "modified":"2017-11-08T10:43:00.000Z",
                "api_version":"0.1",
                "uuid":"83664060-a69f-0135-324a-0242ac120005",
                "base_url":"https://sandbox7.fairdomhub.org"
            }
        },
        "jsonapi":{
            "version":"1.0"
        }
    },

    {
        "data":{
            "id":"20",
            "type":"publications",
            "attributes":{
                "title":"Storing and annotating of kinetic data.",
                "journal":"In Silico Biol",
                "published_date":"2007-09-14",
                "doi":null,
                "pubmed_id":17822389,
                "abstract":"This paper briefly describes the SABIO-RK database model for the storage of reaction kinetics information and the guidelines followed within the SABIO-RK project to annotate the kinetic data. Such annotations support the definition of cross links to other related databases and augment the semantics of the data stored in the database.",
                "citation":"In Silico Biol. 2007;7(2 Suppl):S37-44.",
                "link_to_pub":"https://www.ncbi.nlm.nih.gov/pubmed/17822389",
                "authors":[
                    "I. Rojas",
                    "M. Golebiewski",
                    "R. Kania",
                    "O. Krebs",
                    "S. Mir",
                    "A. Weidemann",
                    "Ulrike Wittig"
                ]
            },
            "relationships":{
                "people":{
                    "data":[
                        {
                            "id":"35",
                            "type":"people"
                        }
                    ]
                },
                "projects":{
                    "data":[
                        {
                            "id":"3",
                            "type":"projects"
                        }
                    ]
                },
                "investigations":{
                    "data":[

                    ]
                },
                "studies":{
                    "data":[

                    ]
                },
                "assays":{
                    "data":[

                    ]
                },
                "data_files":{
                    "data":[

                    ]
                },
                "models":{
                    "data":[

                    ]
                },
                "publications":{
                    "data":[

                    ]
                },
                "presentations":{
                    "data":[

                    ]
                },
                "events":{
                    "data":[

                    ]
                }
            },
            "links":{
                "self":"/publications/20"
            },
            "meta":{
                "created":"2017-11-08T10:43:46.000Z",
                "modified":"2017-11-08T10:43:46.000Z",
                "api_version":"0.1",
                "uuid":"9f4b6d10-a69f-0135-324b-0242ac120005",
                "base_url":"https://sandbox7.fairdomhub.org"
            }
        },
        "jsonapi":{
            "version":"1.0"
        }
    },
    {
        "data":{
            "id":"21",
            "type":"publications",
            "attributes":{
                "title":"Analysis and comparison of metabolic pathway databases.",
                "journal":"Brief Bioinform",
                "published_date":"2001-07-24",
                "doi":null,
                "pubmed_id":11465731,
                "abstract":"Enormous amounts of data result from genome sequencing projects and new experimental methods. Within this tremendous amount of genomic data 30-40 per cent of the genes being identified in an organism remain unknown in terms of their biological function. As a consequence of this lack of information the overall schema of all the biological functions occurring in a specific organism cannot be properly represented. To understand the functional properties of the genomic data more experimental data must be collected. A pathway database is an effort to handle the current knowledge of biochemical pathways and in addition can be used for interpretation of sequence data. Some of the existing pathway databases can be interpreted as detailed functional annotations of genomes because they are tightly integrated with genomic information. However, experimental data are often lacking in these databases. This paper summarises a list of pathway databases and some of their corresponding biological databases, and also focuses on information about the content and the structure of these databases, the organisation of the data and the reliability of stored information from a biological point of view. Moreover, information about the representation of the pathway data and tools to work with the data are given. Advantages and disadvantages of the analysed databases are pointed out, and an overview to biological scientists on how to use these pathway databases is given.",
                "citation":"Brief Bioinform. 2001 May;2(2):126-42.",
                "link_to_pub":"https://www.ncbi.nlm.nih.gov/pubmed/11465731",
                "authors":[
                    "Ulrike Wittig",
                    "A. De Beuckelaer"
                ]
            },
            "relationships":{
                "people":{
                    "data":[
                        {
                            "id":"35",
                            "type":"people"
                        }
                    ]
                },
                "projects":{
                    "data":[
                        {
                            "id":"3",
                            "type":"projects"
                        }
                    ]
                },
                "investigations":{
                    "data":[

                    ]
                },
                "studies":{
                    "data":[

                    ]
                },
                "assays":{
                    "data":[

                    ]
                },
                "data_files":{
                    "data":[

                    ]
                },
                "models":{
                    "data":[

                    ]
                },
                "publications":{
                    "data":[

                    ]
                },
                "presentations":{
                    "data":[

                    ]
                },
                "events":{
                    "data":[

                    ]
                }
            },
            "links":{
                "self":"/publications/21"
            },
            "meta":{
                "created":"2017-11-08T10:45:17.000Z",
                "modified":"2017-11-08T10:45:17.000Z",
                "api_version":"0.1",
                "uuid":"d56c48d0-a69f-0135-324b-0242ac120005",
                "base_url":"https://sandbox7.fairdomhub.org"
            }
        },
        "jsonapi":{
            "version":"1.0"
        }
    },

    {
        "data":{
            "id":"22",
            "type":"publications",
            "attributes":{
                "title":"Classification of chemical compounds to support complex queries in a pathway database.",
                "journal":"Comp Funct Genomics",
                "published_date":"2008-07-17",
                "doi":null,
                "pubmed_id":18629066,
                "abstract":"Data quality in biological databases has become a topic of great discussion. To provide high quality data and to deal with the vast amount of biochemical data, annotators and curators need to be supported by software that carries out part of their work in an (semi-) automatic manner. The detection of errors and inconsistencies is a part that requires the knowledge of domain experts, thus in most cases it is done manually, making it very expensive and time-consuming. This paper presents two tools to partially support the curation of data on biochemical pathways. The tool enables the automatic classification of chemical compounds based on their respective SMILES strings. Such classification allows the querying and visualization of biochemical reactions at different levels of abstraction, according to the level of detail at which the reaction participants are described. Chemical compounds can be classified in a flexible manner based on different criteria. The support of the process of data curation is provided by facilitating the detection of compounds that are identified as different but that are actually the same. This is also used to identify similar reactions and, in turn, pathways.",
                "citation":"Comp Funct Genomics. 2004;5(2):156-62. doi: 10.1002/cfg.387.",
                "link_to_pub":"https://www.ncbi.nlm.nih.gov/pubmed/18629066",
                "authors":[
                    "Ulrike Wittig",
                    "A. Weidemann",
                    "R. Kania",
                    "C. Peiss",
                    "I. Rojas"
                ]
            },
            "relationships":{
                "people":{
                    "data":[
                        {
                            "id":"35",
                            "type":"people"
                        }
                    ]
                },
                "projects":{
                    "data":[
                        {
                            "id":"3",
                            "type":"projects"
                        }
                    ]
                },
                "investigations":{
                    "data":[

                    ]
                },
                "studies":{
                    "data":[

                    ]
                },
                "assays":{
                    "data":[

                    ]
                },
                "data_files":{
                    "data":[

                    ]
                },
                "models":{
                    "data":[

                    ]
                },
                "publications":{
                    "data":[

                    ]
                },
                "presentations":{
                    "data":[

                    ]
                },
                "events":{
                    "data":[

                    ]
                }
            },
            "links":{
                "self":"/publications/22"
            },
            "meta":{
                "created":"2017-11-08T10:45:50.000Z",
                "modified":"2017-11-08T10:45:50.000Z",
                "api_version":"0.1",
                "uuid":"e8c0daa0-a69f-0135-324a-0242ac120005",
                "base_url":"https://sandbox7.fairdomhub.org"
            }
        },
        "jsonapi":{
            "version":"1.0"
        }
    },

    {
        "data":{
            "id":"23",
            "type":"publications",
            "attributes":{
                "title":"Developing a protein-interactions ontology.",
                "journal":"Comp Funct Genomics",
                "published_date":"2008-07-17",
                "doi":null,
                "pubmed_id":18629092,
                "abstract":null,
                "citation":"Comp Funct Genomics. 2003;4(1):85-9. doi: 10.1002/cfg.244.",
                "link_to_pub":"https://www.ncbi.nlm.nih.gov/pubmed/18629092",
                "authors":[
                    "E. Ratsch",
                    "J. Schultz",
                    "J. Saric",
                    "P. C. Lavin",
                    "Ulrike Wittig",
                    "U. Reyle",
                    "I. Rojas"
                ]
            },
            "relationships":{
                "people":{
                    "data":[
                        {
                            "id":"35",
                            "type":"people"
                        }
                    ]
                },
                "projects":{
                    "data":[
                        {
                            "id":"3",
                            "type":"projects"
                        }
                    ]
                },
                "investigations":{
                    "data":[
                        {
                            "id":"3",
                            "type":"investigations"
                        }
                    ]
                },
                "studies":{
                    "data":[

                    ]
                },
                "assays":{
                    "data":[

                    ]
                },
                "data_files":{
                    "data":[

                    ]
                },
                "models":{
                    "data":[

                    ]
                },
                "publications":{
                    "data":[

                    ]
                },
                "presentations":{
                    "data":[

                    ]
                },
                "events":{
                    "data":[

                    ]
                }
            },
            "links":{
                "self":"/publications/23"
            },
            "meta":{
                "created":"2017-11-08T10:46:20.000Z",
                "modified":"2017-11-08T10:46:20.000Z",
                "api_version":"0.1",
                "uuid":"fac2e7f0-a69f-0135-324a-0242ac120005",
                "base_url":"https://sandbox7.fairdomhub.org"
            }
        },
        "jsonapi":{
            "version":"1.0"
        }
    },

    {
        "data":{
            "id":"24",
            "type":"publications",
            "attributes":{
                "title":"Notes on the use of ontologies in the biochemical domain.",
                "journal":"In Silico Biol",
                "published_date":"2004-04-20",
                "doi":null,
                "pubmed_id":15089756,
                "abstract":"In this paper we aim at presenting the main flavours and uses that are given to the term ontology in the bio-domains. The paper does not intend to be a thorough review of the existing work in the area. It highlights the uses that are given to ontologies in the Scientific Databases and Visualisation Group at EML Research, in Heidelberg.",
                "citation":"In Silico Biol. 2004;4(1):89-96. Epub 2004 Mar 15.",
                "link_to_pub":"https://www.ncbi.nlm.nih.gov/pubmed/15089756",
                "authors":[
                    "I. Rojas",
                    "E. Ratsch",
                    "J. Saric",
                    "Ulrike Wittig"
                ]
            },
            "relationships":{
                "people":{
                    "data":[
                        {
                            "id":"35",
                            "type":"people"
                        }
                    ]
                },
                "projects":{
                    "data":[
                        {
                            "id":"3",
                            "type":"projects"
                        }
                    ]
                },
                "investigations":{
                    "data":[

                    ]
                },
                "studies":{
                    "data":[

                    ]
                },
                "assays":{
                    "data":[

                    ]
                },
                "data_files":{
                    "data":[

                    ]
                },
                "models":{
                    "data":[

                    ]
                },
                "publications":{
                    "data":[

                    ]
                },
                "presentations":{
                    "data":[

                    ]
                },
                "events":{
                    "data":[

                    ]
                }
            },
            "links":{
                "self":"/publications/24"
            },
            "meta":{
                "created":"2017-11-08T10:47:03.000Z",
                "modified":"2017-11-08T10:47:03.000Z",
                "api_version":"0.1",
                "uuid":"14bb1cf0-a6a0-0135-3249-0242ac120005",
                "base_url":"https://sandbox7.fairdomhub.org"
            }
        },
        "jsonapi":{
            "version":"1.0"
        }
    },

    {
        "data":{
            "id":"25",
            "type":"publications",
            "attributes":{
                "title":"A database system for the analysis of biochemical pathways.",
                "journal":"In Silico Biol",
                "published_date":"2002-06-18",
                "doi":null,
                "pubmed_id":12066842,
                "abstract":"To provide support for the analysis of biochemical pathways a database system based on a model that represents the characteristics of the domain is needed. This domain has proven to be difficult to model by using conventional data modelling techniques. We are building an ontology for biochemical pathways, which acts as the basis for the generation of a database on the same domain, allowing the definition of complex queries and complex data representation. The ontology is used as a modelling and analysis tool which allows the expression of complex semantics based on a first-order logic representation language. The induction capabilities of the system can help the scientist in formulating and testing research hypotheses that are difficult to express with the standard relational database mechanisms. An ontology representing the shared formalisation of the knowledge in a scientific domain can also be used as data integration tool clarifying the mapping of concepts to the developers of different databases. In this paper we describe the general structure of our system, concentrating on the ontology-based database as the key component of the system.",
                "citation":"In Silico Biol. 2002;2(2):75-86.",
                "link_to_pub":"https://www.ncbi.nlm.nih.gov/pubmed/12066842",
                "authors":[
                    "I. Rojas",
                    "L. Bernardi",
                    "E. Ratsch",
                    "R. Kania",
                    "Ulrike Wittig",
                    "J. Saric"
                ]
            },
            "relationships":{
                "people":{
                    "data":[
                        {
                            "id":"35",
                            "type":"people"
                        }
                    ]
                },
                "projects":{
                    "data":[
                        {
                            "id":"3",
                            "type":"projects"
                        }
                    ]
                },
                "investigations":{
                    "data":[

                    ]
                },
                "studies":{
                    "data":[

                    ]
                },
                "assays":{
                    "data":[

                    ]
                },
                "data_files":{
                    "data":[

                    ]
                },
                "models":{
                    "data":[

                    ]
                },
                "publications":{
                    "data":[

                    ]
                },
                "presentations":{
                    "data":[

                    ]
                },
                "events":{
                    "data":[

                    ]
                }
            },
            "links":{
                "self":"/publications/25"
            },
            "meta":{
                "created":"2017-11-08T10:47:31.000Z",
                "modified":"2017-11-08T10:47:31.000Z",
                "api_version":"0.1",
                "uuid":"2557e620-a6a0-0135-324b-0242ac120005",
                "base_url":"https://sandbox7.fairdomhub.org"
            }
        },
        "jsonapi":{
            "version":"1.0"
        }
    },

    {
        "data":{
            "id":"26",
            "type":"publications",
            "attributes":{
                "title":"Excemplify: a flexible template based solution, parsing and managing data in spreadsheets for experimentalists.",
                "journal":"J Integr Bioinform",
                "published_date":"2013-04-04",
                "doi":null,
                "pubmed_id":23549603,
                "abstract":"In systems biology, quantitative experimental data is the basis of building mathematical models. In most of the cases, they are stored in Excel files and hosted locally. To have a public database for collecting, retrieving and citing experimental raw data as well as experimental conditions is important for both experimentalists and modelers. However, the great effort needed in the data handling procedure and in the data submission procedure becomes the crucial limitation for experimentalists to contribute to a database, thereby impeding the database to deliver its benefit. Moreover, manual copy and paste operations which are commonly used in those procedures increase the chance of making mistakes. Excemplify, a web-based application, proposes a flexible and adaptable template-based solution to solve these problems. Comparing to the normal template based uploading approach, which is supported by some public databases, rather than predefining a format that is potentiall impractical, Excemplify allows users to create their own experiment-specific content templates in different experiment stages and to build corresponding knowledge bases for parsing. Utilizing the embedded knowledge of used templates, Excemplify is able to parse experimental data from the initial setup stage and generate following stages spreadsheets automatically. The proposed solution standardizes the flows of data traveling according to the standard procedures of applying the experiment, cuts down the amount of manual effort and reduces the chance of mistakes caused by manual data handling. In addition, it maintains the context of meta-data from the initial preparation manuscript and improves the data consistency. It interoperates and complements RightField and SEEK as well.",
                "citation":"J Integr Bioinform. 2013 Apr 3;10(2):220. doi: 10.2390/biecoll-jib-2013-220.",
                "link_to_pub":"https://www.ncbi.nlm.nih.gov/pubmed/23549603",
                "authors":[
                    "L. Shi",
                    "L. Jong",
                    "Ulrike Wittig",
                    "P. Lucarelli",
                    "M. Stepath",
                    "S. Mueller",
                    "L. A. D'Alessandro",
                    "U. Klingmuller",
                    "Wolfgang Müller"
                ]
            },
            "relationships":{
                "people":{
                    "data":[
                        {
                            "id":"33",
                            "type":"people"
                        },
                        {
                            "id":"35",
                            "type":"people"
                        }
                    ]
                },
                "projects":{
                    "data":[
                        {
                            "id":"3",
                            "type":"projects"
                        }
                    ]
                },
                "investigations":{
                    "data":[

                    ]
                },
                "studies":{
                    "data":[
                        {
                            "id":"9",
                            "type":"studies"
                        }
                    ]
                },
                "assays":{
                    "data":[

                    ]
                },
                "data_files":{
                    "data":[

                    ]
                },
                "models":{
                    "data":[

                    ]
                },
                "publications":{
                    "data":[

                    ]
                },
                "presentations":{
                    "data":[

                    ]
                },
                "events":{
                    "data":[

                    ]
                }
            },
            "links":{
                "self":"/publications/26"
            },
            "meta":{
                "created":"2017-11-08T10:48:07.000Z",
                "modified":"2017-11-08T10:48:07.000Z",
                "api_version":"0.1",
                "uuid":"3ae8f750-a6a0-0135-324b-0242ac120005",
                "base_url":"https://sandbox7.fairdomhub.org"
            }
        },
        "jsonapi":{
            "version":"1.0"
        }
    },

    {
        "data":{
            "id":"27",
            "type":"publications",
            "attributes":{
                "title":"SYCAMORE--a systems biology computational analysis and modeling research environment",
                "journal":"Bioinformatics",
                "published_date":"2008-06-13",
                "doi":"10.1093/bioinformatics/btn207",
                "pubmed_id":null,
                "abstract":"SYCAMORE is a browser-based application that facilitates construction, simulation and analysis of kinetic models in systems biology. Thus, it allows e.g. database supported modelling, basic model checking and the estimation of unknown kinetic parameters based on protein structures. In addition, it offers some guidance in order to allow non-expert users to perform basic computational modelling tasks.",
                "citation":"Bioinformatics 24(12) : 1463",
                "link_to_pub":"https://www.ncbi.nlm.nih.gov/pubmed/",
                "authors":[
                    "A. Weidemann",
                    "S. Richter",
                    "M. Stein",
                    "S. Sahle",
                    "R. Gauges",
                    "R. Gabdoulline",
                    "I. Surovtsova",
                    "N. Semmelrock",
                    "B. Besson",
                    "I. Rojas",
                    "R. Wade",
                    "U. Kummer"
                ]
            },
            "relationships":{
                "people":{
                    "data":[
                        {
                            "id":"35",
                            "type":"people"
                        }
                    ]
                },
                "projects":{
                    "data":[
                        {
                            "id":"3",
                            "type":"projects"
                        }
                    ]
                },
                "investigations":{
                    "data":[

                    ]
                },
                "studies":{
                    "data":[

                    ]
                },
                "assays":{
                    "data":[

                    ]
                },
                "data_files":{
                    "data":[

                    ]
                },
                "models":{
                    "data":[

                    ]
                },
                "publications":{
                    "data":[

                    ]
                },
                "presentations":{
                    "data":[

                    ]
                },
                "events":{
                    "data":[

                    ]
                }
            },
            "links":{
                "self":"/publications/27"
            },
            "meta":{
                "created":"2017-11-08T10:50:56.000Z",
                "modified":"2017-11-08T11:12:58.000Z",
                "api_version":"0.1",
                "uuid":"9f573190-a6a0-0135-3248-0242ac120005",
                "base_url":"https://sandbox7.fairdomhub.org"
            }
        },
        "jsonapi":{
            "version":"1.0"
        }
    },

    {
        "data":{
            "id":"28",
            "type":"publications",
            "attributes":{
                "title":"Big data: The future of biocuration",
                "journal":"Nature",
                "published_date":"2008-09-04",
                "doi":"10.1038/455047a",
                "pubmed_id":null,
                "abstract":null,
                "citation":"Nature 455(7209) : 47",
                "link_to_pub":"https://www.ncbi.nlm.nih.gov/pubmed/",
                "authors":[
                    "Doug Howe",
                    "Maria Costanzo",
                    "Petra Fey",
                    "Takashi Gojobori",
                    "Linda Hannick",
                    "Winston Hide",
                    "David P. Hill",
                    "Renate Kania",
                    "Mary Schaeffer",
                    "Susan St Pierre",
                    "Simon Twigger",
                    "Owen White",
                    "Seung Yon Rhee"
                ]
            },
            "relationships":{
                "people":{
                    "data":[
                        {
                            "id":"35",
                            "type":"people"
                        }
                    ]
                },
                "projects":{
                    "data":[
                        {
                            "id":"3",
                            "type":"projects"
                        }
                    ]
                },
                "investigations":{
                    "data":[

                    ]
                },
                "studies":{
                    "data":[

                    ]
                },
                "assays":{
                    "data":[
                        {
                            "id":"6",
                            "type":"assays"
                        }
                    ]
                },
                "data_files":{
                    "data":[
                        {
                            "id":"12",
                            "type":"data_files"
                        },
                        {
                            "id":"13",
                            "type":"data_files"
                        }
                    ]
                },
                "models":{
                    "data":[

                    ]
                },
                "publications":{
                    "data":[

                    ]
                },
                "presentations":{
                    "data":[

                    ]
                },
                "events":{
                    "data":[

                    ]
                }
            },
            "links":{
                "self":"/publications/28"
            },
            "meta":{
                "created":"2017-11-08T10:52:15.000Z",
                "modified":"2017-11-08T10:52:23.000Z",
                "api_version":"0.1",
                "uuid":"ce762510-a6a0-0135-3249-0242ac120005",
                "base_url":"https://sandbox7.fairdomhub.org"
            }
        },
        "jsonapi":{
            "version":"1.0"
        }
    },

    {
        "data":{
            "id":"29",
            "type":"publications",
            "attributes":{
                "title":"Metabolic model of central carbon and energy metabolisms of growing Arabidopsis thaliana in relation to sucrose translocation",
                "journal":"BMC Plant Biol",
                "published_date":"2016-12-01",
                "doi":"10.1186/s12870-016-0868-3",
                "pubmed_id":null,
                "abstract":null,
                "citation":"BMC Plant Biol 16(1) : e00669",
                "link_to_pub":"https://www.ncbi.nlm.nih.gov/pubmed/",
                "authors":[
                    "Maksim Zakhartsev",
                    "Irina Medvedeva",
                    "Yury Orlov",
                    "Ilya Akberdin",
                    "Olga Krebs",
                    "Waltraud X. Schulze"
                ]
            },
            "relationships":{
                "people":{
                    "data":[
                        {
                            "id":"35",
                            "type":"people"
                        },
                        {
                            "id":"38",
                            "type":"people"
                        }
                    ]
                },
                "projects":{
                    "data":[
                        {
                            "id":"3",
                            "type":"projects"
                        }
                    ]
                },
                "investigations":{
                    "data":[

                    ]
                },
                "studies":{
                    "data":[

                    ]
                },
                "assays":{
                    "data":[

                    ]
                },
                "data_files":{
                    "data":[

                    ]
                },
                "models":{
                    "data":[

                    ]
                },
                "publications":{
                    "data":[

                    ]
                },
                "presentations":{
                    "data":[

                    ]
                },
                "events":{
                    "data":[

                    ]
                }
            },
            "links":{
                "self":"/publications/29"
            },
            "meta":{
                "created":"2017-11-10T09:59:35.000Z",
                "modified":"2017-11-10T09:59:48.000Z",
                "api_version":"0.1",
                "uuid":"c7dc4c80-a82b-0135-3249-0242ac120005",
                "base_url":"https://sandbox7.fairdomhub.org"
            }
        },
        "jsonapi":{
            "version":"1.0"
        }
    }
]