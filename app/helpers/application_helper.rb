module ApplicationHelper

  # Returns the full title on a per-page basis.
  def full_title(page_title = '')
    base_title = "RoR Test"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end
end

# email validation
def is_a_valid_email?(email)
  (email =/\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i)
end