class User < ApplicationRecord
  
  
    #has_many :microposts, dependent: :destroy
    has_many :posts, dependent: :destroy
    has_many :comments, through: :posts, dependent: :destroy
    
    #user as a follower  #if a table has any columns whose values consist of ID values for another table , it is a foreign keys.  
    #the corresponding model should have a belongs_to for each
  has_many :active_relationships,  class_name:  "Relationship",
                                   foreign_key: "follower_id",
                                   dependent:   :destroy  
  has_many :following, through: :active_relationships, source: :followed


   #user as being followed
  has_many :passive_relationships, class_name:  "Relationship", # classname = real class is 
                                  foreign_key: "followed_id",
                                  dependent:   :destroy  # when user is destroyed, the relationship is also destory
  has_many :followers, through: :passive_relationships, source: :follower # this is just for reading easier 
  

   validates :name, length: { maximum: 50 }, presence: true
  
  
  before_save { self.email = self.email.downcase }
  
  #email reqularexpress
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email,length: { maximum: 255}, 
                   presence: true,
                   format: { with: VALID_EMAIL_REGEX },
                   uniqueness: { case_sensitive: false }
    
    
                   
    has_secure_password    
    validates :password, presence: true, length: { minimum: 6 }, allow_nil: true

    #self.password==self.password_confirmation
   # validates :password_confirmation,  presence: true,length: { minimum: 6 },allow_nil: true
    
    # Returns the hash digest of the given string.
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end
  
  # Defines a proto-feed.
  # See "Following users" for the full implementation.
  def feed
  following_ids = "SELECT followed_id FROM relationships
                    WHERE  follower_id = :user_id"
                    
    # Micropost.where("user_id = ?", id)
    # = Micropost.where("user_id IN (?) OR user_id = ?", following_ids, id)
    Micropost.where("user_id IN (:following_ids) OR user_id = :user_id",
                following_ids: following_ids, user_id: id)
  end
  
  # Follows a user.
  def follow(other_user)
    following << other_user
  end

  # Unfollows a user.
  def unfollow(other_user)
    following.delete(other_user)
  end

  # Returns true if the current user is following the other user.
  def following?(other_user)
    following.include?(other_user)
  end
  

end
