class SessionsController < ApplicationController
  def new
  end
  
  def create
    # debugger
      user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      # Log the user in and redirect to the user's show page.
      flash[:success] = "You logged in!"    
      log_in user
      redirect_to user_url(current_user) # to user/id
      #redirect_to user
    else
      flash.now[:danger] = 'Invalid email/password combination' # Not quite right!
      render 'new'
    end
  end
  
  def destroy
    log_out
    redirect_to root_path
  end
  
end
