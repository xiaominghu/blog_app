class MicropostsController < ApplicationController

 before_action :logged_in_user, only: [:create, :destroy]
 before_action :correct_user,   only: :destroy

  
  def destroy
     @micropost.destroy
    flash[:success] = "Micropost is deleted"
    redirect_to user_path(current_user)
  end

def create    
     @micropost = current_user.microposts.build(micropost_params)
     if @micropost.save
      flash[:success] = "The comment is saved!" 
      redirect_to user_path(current_user)
      else
        #@feed_items = current_user.feed.paginate(page: params[:page])
        @feed_items = []
        render 'static_pages/home'
      end
  end

  private
    def micropost_params
      params.require(:micropost).permit(:content)
    end
    
    def correct_user
      @micropost = current_user.microposts.find_by(id: params[:id])
      redirect_to root_url if @micropost.nil?
    end
    
end 
   