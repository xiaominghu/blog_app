class CommentsController < ApplicationController

  def create
    user_id = current_user.id
    @post = Post.find(params[:post_id])
    content = params[:comment][:content]
    @post.comments.create(user_id:user_id, content:content)
    redirect_to post_path(@post)  
  end

  def destroy
    @comment = Comment.find(params[:id])
    @post = Post.find(@comment.post_id)
    @comment.destroy
    redirect_to post_path(@post)
  end
  
end
