class UsersController < ApplicationController
  
  before_action :logged_in_user, only: [:index, :edit, :update, :destory]
  before_action :correct_user,   only: [:edit, :update]
  before_action :admin_user,     only: :destroy
  
  def new
    @user = User.new   
  end

  def index
    @users = User.paginate(page: params[:page])
   # @users = User.all
  end

  def show
     @user = User.find(params[:id])
      # debugger
     # @microposts = @user.microposts.paginate(page: params[:page])
     @posts = @user.posts.paginate(page: params[:page])
     # @micropost = current_user.microposts.build if logged_in?
       if(!@user.profile.nil?)
        @user_profile = @user.profile.to_hash
       end

  end
  
 def create
    @user = User.new(user_params)
    if @user.save
      log_in @user
      flash[:success] = "Welcome to the Sample App!"   
      # https://getbootstrap.com/docs/4.0/components/alerts/
      # message type
       redirect_to   # to user/id
      #redirect_to users_url  (to /users)
      # debugger
    else
      render 'new'
    end
  end
  
  def edit
    @user = User.find(params[:id])
  end
  
  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "The new profile is saved!" 
      redirect_to user_url(@user)  
    else
      render 'edit'
    end
    
  end
  
  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to users_url
  end
  
  
  def following
 
    @title = "Following"
    @user  = User.find(params[:id])
    @users = User.find(params[:id]).following
    render 'show_follow'
  end
  
  def followers
    @title = "Following"
    @user  = User.find(params[:id])
    @users = User.find(params[:id]).followers
    render 'show_follow'
  end
  
  def posts
    @title = "Posts"
    @user  = User.find(params[:id])
    @posts = User.find(params[:id]).posts
    render 'show_posts'
  end
  
  def usergroups
    @title = "CSV"
    @usergroups  = Usergroup.all    
    # render 'show_usergroups'

  respond_to do |format|
    format.html { render 'show_usergroups' }
    format.js
    format.csv { send_data @usergroups.to_csv }
  end
    
  end
  
  
  def import
    @user = current_user
    Usergroup.import(params[:file])    
    flash[:success] = "CSV File is uploaded to database!"
    redirect_to usergroups_user_path(@user)
  end
  

  private
    def user_params
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation)
    end
   
    
    # Confirms the correct user.
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
    
    # Confirms an admin user.
    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end
  
end
