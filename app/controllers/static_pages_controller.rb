
class StaticPagesController < ApplicationController
  
  def home
      if logged_in?
      @posts= current_user.posts.paginate(page: params[:page])
      @user = current_user
      else
      @posts = Post.all.paginate(page: params[:page])
      end
  end

  def help
    file = File.read('jsondata.json')
    
    render :json => file
   
  end
  
  def about
    url = 'https://api.spotify.com/v1/search?type=artist&q=tycho'
    uri = URI(url)
      
     # response = Net::HTTP.get(uri)
     # @reply = JSON.parse(response)
    
    
    file = File.read('jsondata.json')
    @data_hash = JSON.parse(file)

    response = HTTParty.get(url)
    @reply =  response.parsed_response  

    
    # response = RestClient.get(url)
    # @reply = JSON.parse(response) 
    
     @user = User.find(1)
     
     @user_profile = @user.profile
  end
  
   def contact
  end
  
end
