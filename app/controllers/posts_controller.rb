class PostsController < ApplicationController
  
  before_action :logged_in_user, only: [:new, :edit, :create, :destroy]
  before_action :correct_user,   only: :destroy
  
   def show
    @post = Post.find(params[:id])
    @user = User.find(@post.user_id)
    @comments = @post.comments
    @comment = Comment.new
  end
  
  def new
    @user=current_user
    @post=Post.new
  end
  
  def create    
     @post = current_user.posts.build(post_params)
     if @post.save
      flash[:success] = "The post is saved!" 
      redirect_to user_path(current_user)
      else
        @user=current_user
        redirect_to new_post_url
      end
  end
  
   def edit
    @post = Post.find(params[:id])
  end
  
  def update
    @post = Post.find(params[:id])
    if @post.update_attributes(post_params)
      flash[:success] = "The bolg is updated!" 
      redirect_to post_url(@post) 
    else
      render 'edit'
    end
    
  end
  
   def destroy
    @post.destroy
    flash[:success] = "The post is deleted"
    redirect_to root_url
  end
  
  private
    def post_params
      params.require(:post).permit(:title,:content)
    end
    
    def correct_user
      @post = current_user.posts.find_by(id: params[:id])
      redirect_to root_url if @post.nil?
    end

  
end
