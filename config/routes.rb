Rails.application.routes.draw do
  
  resources :usergroups
  
  get 'comments/create'
  get 'comments/destory'
  
  resources :comments
  
  resources :posts
  resources :users do
    member do
      get :following, :followers , :posts, :usergroups
    end
  end
  resources :microposts
  resources :relationships,       only: [:create, :destroy]
  
  
 resources :users do
  collection do
    post :import   #this defines : import_users POST   /users/import(.:format)   users#import
  end
end


  resources :publications
  
  
  #static_pages_controller
  get  '/about',   to: 'static_pages#about'
  get  '/help',   to: 'static_pages#help'
  get  '/search',   to: 'static_pages#search'
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
  #user_controller
   get  '/signup', to: 'users#new'
   post '/signup',  to: 'users#create'
   
   #session_controller
   get '/login', to: 'sessions#new'
   post '/login', to: 'sessions#create'
   delete '/logout', to: 'sessions#destroy'
   
   #micropost_controller
   post '/users', to: 'microposts#create'
  
  #root 'application#hello'
   root 'static_pages#home'
  
end
